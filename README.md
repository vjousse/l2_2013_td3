# Instructions

- Déposer un fichier du type `nom-prenom.txt` sur ce dépôt
- Le fichier doit contenir votre votre nom, votre prenom et votre email
- Un fichier par personne

Un example de fichier est [accessible ici](https://bitbucket.org/vjousse/l2_2013_td3/src/df71878f751bf468a8ec77395a081af4ca7e3d8e/jousse-vincent.txt?at=master).

Vous aurez certainement besoin des commandes suivantes :

- `git clone`
- `git add`
- `git commit`
- `git pull`
- `git push`
